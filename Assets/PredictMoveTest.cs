﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictMoveTest : MonoBehaviour
{
    [SerializeField]
    private float circleRadius;
    [SerializeField]
    private GameObject targetGO;
    [SerializeField]
    private GameObject bulletPf;
    [SerializeField]
    private GameObject markerGO;
    [SerializeField]
    private float predictionTime;


    [SerializeField]
    private CircleMovement circMovement = new CircleMovement();
    [SerializeField]
    private GameObject inputProviderGO;
    private InputProvider inputProv;

    private void Awake()
    {
        inputProv = inputProviderGO.GetComponent<InputProvider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        circMovement.Init(gameObject, targetGO.transform, inputProv);

        //InvokeRepeating("FireBullet", 0, 2);
    }

    private void Update()
    {
        circMovement.Radius = circleRadius;
        circMovement.UpdateInput();
    }

    private void FixedUpdate()
    {
        circMovement.UpdateMovement();
        //markerGO.transform.localPosition = PredictTargetLocalPos(predictionTime, circMovement.GetCircleMovementStateCopy());
    }


    private void FireBullet()
    {
        Vector3 predLocalPos    = markerGO.transform.localPosition;
        Vector3 predWorldPos    = transform.TransformPoint(predLocalPos);
        Vector3 bulletDir       = predWorldPos - transform.position;
        Quaternion bulletRot    = Quaternion.LookRotation(bulletDir);
        GameObject bullet       = Instantiate(bulletPf, transform.position, bulletRot);
        var bulletCon           = bullet.GetComponent<SimpleBulletController>();
        float speed             = circleRadius / predictionTime;
        bulletCon.DeltaPosition = speed * Time.fixedDeltaTime;
    }

    private Vector3 PredictTargetLocalPos(float predictionTime, CircleMovementState circMovState)
    {
        int predCount = Mathf.FloorToInt(predictionTime / Time.fixedDeltaTime);

        Quaternion initialRot = circMovState.Rot;

        for (int i = 0; i < predCount; i++)
        {
            //CircleMovement.UpdateCircleMovementState(circMovState);
        }

        float predAngleDelta = (circMovState.Rot*Quaternion.Inverse(initialRot)).eulerAngles.y;

        // Current rotation is not applied here as it is for a local child pos
        Vector3 predPos = UtilityFunctions.CalculatePositionOnXZCircle(circleRadius, predAngleDelta - 90);

        return predPos;
    }

}
