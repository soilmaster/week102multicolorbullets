using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

public static class UtilityFunctions
{
    /// <summary>
    /// Calculate a list of 3D points on a circle of a know radius around a known transform (aligned to the xz plane)
    /// </summary>
    /// <param name="parentTransform"></param>
    /// <param name="inputRadius"></param>
    /// <param name="noCircPoints"></param>
    /// <returns></returns>
    public static List<Vector3> GetLocalCirclePoints(Transform parentTransform, float inputRadius, int noCircPoints) 
    {
        float radius    = Mathf.Abs(inputRadius); 
        float segAngle  = 360f/noCircPoints;
        List<Vector3> circLinePnts = new List<Vector3>();
        for (int circPntCtr = 0; circPntCtr <= noCircPoints; circPntCtr++)
        {
            float curAngle = segAngle * circPntCtr;
            Vector3 curCircPnt = new Vector3
            (
                radius*Mathf.Cos(Mathf.Deg2Rad*curAngle),
                0f,
                radius*Mathf.Sin(Mathf.Deg2Rad*curAngle)
            );            
            curCircPnt = parentTransform.rotation*curCircPnt;
            curCircPnt = parentTransform.position+curCircPnt;
            circLinePnts.Add(curCircPnt);
        }
        return circLinePnts;
    }


    /// <summary>
    /// Returns a 3D point that lies on a XZ plane aligned circle of a known radius at a given angle.
    /// Circle is centered around world origin
    /// </summary>
    /// <param name="circleRadius"></param>
    /// <param name="angle">Angle in degrees</param>
    /// <returns></returns>
    public static Vector3 CalculatePositionOnXZCircle(float circleRadius, float angleDegrees)
    {
        return new Vector3( circleRadius*Mathf.Sin(Mathf.Deg2Rad*angleDegrees), 
                            0, 
                            circleRadius*Mathf.Cos(Mathf.Deg2Rad*angleDegrees));
    }

    /// <summary>
    /// Calculates the position of an object travelling along a 2D trajectory in 3D space
    /// </summary>
    /// <param name="startPos"></param>
    /// <param name="targetPos"></param>
    /// <param name="timeElapsed"></param>
    /// <param name="elevAngle"></param>
    /// <param name="targetAngle"></param>
    /// <returns></returns>
    public static Vector3 CalculateArcPosition(
        Vector3 startPos,
        Vector3 targetPos,
        float timeElapsed,
        float elevAngle,
        float targetAngle
    )
    {
        Vector3 resultantDisp3D = targetPos - startPos;
        float resultantDispXZ   = (new Vector2(resultantDisp3D.x, resultantDisp3D.z)).magnitude;
        float initialVelocity   = Mathf.Sqrt( resultantDispXZ *
                                            -Physics.gravity.y /
                                            Mathf.Sin(Mathf.Deg2Rad * elevAngle * 2));

        float dispX =   initialVelocity *
                        Mathf.Cos(Mathf.Deg2Rad * elevAngle) *
                        Mathf.Sin(Mathf.Deg2Rad * targetAngle) *
                        timeElapsed +
                        startPos.x;

        float dispY =   initialVelocity *
                        Mathf.Sin(Mathf.Deg2Rad * elevAngle) *
                        timeElapsed -
                        0.5f *
                        -Physics.gravity.y *
                        timeElapsed *
                        timeElapsed +
                        startPos.y;

        float dispZ =   initialVelocity *
                        Mathf.Cos(Mathf.Deg2Rad * elevAngle) *
                        Mathf.Cos(Mathf.Deg2Rad * targetAngle) *
                        timeElapsed +
                        startPos.z;

        Vector3 result = new Vector3(dispX, dispY, dispZ);

        return result;
    }


    private static MethodInfo _clearConsoleMethod;
    private static MethodInfo clearConsoleMethod
    {
        get
        {
            if (_clearConsoleMethod == null)
            {
                Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
                System.Type logEntries = assembly.GetType("UnityEditor.LogEntries");
                _clearConsoleMethod = logEntries.GetMethod("Clear");
            }
            return _clearConsoleMethod;
        }
    }
    public static void ClearLogConsole()
    {
        clearConsoleMethod.Invoke(new object(), null);
    }
}