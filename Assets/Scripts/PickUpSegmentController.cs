﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSegmentController : MonoBehaviour
{

    [SerializeField]
    private GameObject healthPickUp;

    public float ShipPathRadius { get; set; }

    private bool isAppQuitting;

    // Where the center of the Arc lies, as opposed to the transform origin
    private Vector3 SegmentCenter 
    {
         get
         {
            return transform.position + transform.rotation*ArcMeshCenter;
         }
    }

    // When the mesh arc mesh is created this should be set
    public Vector3 ArcMeshCenter { get; set; }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnApplicationQuit()
    {
        isAppQuitting = true;
    }

    void OnDestroy()
    {
        if(!isAppQuitting)
        {
            GameObject pickUp = Instantiate(healthPickUp, SegmentCenter, Quaternion.identity);
            HealthPickUpController pickUpCon = pickUp.GetComponent<HealthPickUpController>();

            GameManagmentProperties.ColorMan.GetColor("HealthPickUp").Active = false;

            float randAngle = Random.Range(0f, 360f);

            //TODO:  Get the ship pat radius in a better way
            Vector3 curCircPnt = new Vector3
            (
                ShipPathRadius*Mathf.Cos(Mathf.Deg2Rad*randAngle),
                0f,
                ShipPathRadius*Mathf.Sin(Mathf.Deg2Rad*randAngle)
            );            
            curCircPnt = transform.parent.transform.rotation*curCircPnt;
            curCircPnt = transform.parent.transform.position+curCircPnt;

            // TODO:  fix this so it dynamically points to where you want it
            pickUpCon.StartMove(curCircPnt);
        }
    }
}
