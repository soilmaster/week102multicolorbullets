using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class BossGridController : MonoBehaviour
{
    public event System.EventHandler<MoveCompleteEventArgs> AddSegmentMoveComplete;
    public event System.EventHandler<MoveCompleteEventArgs> RotateMoveComplete;

    #region Properties

    public float MinGridRadius { get{ return minGridRadius;} }
    [SerializeField]
    private float minGridRadius;
    public float SegmentBuffer { get{ return segmentBuffer;} }
    [SerializeField]
    private float segmentBuffer;

    /// <summary>
    /// Number of layers is internally set based on the kill radius of the boss grid
    /// </summary>
    public int NoOfLayers { get { return noOfLayers; } }
    private int noOfLayers;

    public int NoOfStraights { get { return noOfStraights;} }
    [SerializeField]
    private int noOfStraights;

    public float LayerIncrement { get{ return layerIncrement;} }
    [SerializeField]
    private float layerIncrement;

    public float SegAngle { get { return (360f)/noOfStraights;} }
    public Vector3 GridCenter { get { return boss.transform.position;} }

    public GameObject ShieldSegmentPf { get {return shieldSegmentPf; } }
    [SerializeField]
    private GameObject shieldSegmentPf;

    public Dictionary<int, Dictionary<int, BossGridSegment>> Layers { get { return layers; } }
    private Dictionary<int, Dictionary<int, BossGridSegment>> layers = new Dictionary<int, Dictionary<int, BossGridSegment>>();

    public Dictionary<int, Dictionary<int, BossGridSegment>> Straights { get { return straights; } }
    private Dictionary<int, Dictionary<int, BossGridSegment>> straights = new Dictionary<int, Dictionary<int, BossGridSegment>>();

    public float GridMaxRadius { get; private set; }

    #endregion

    #region Fields
    [SerializeField]
    private int initialPopulatedLayers;
    [SerializeField]
    private GameObject boss;
    private BossController bossCon;
    private GridMoveStack moveStack;

    #endregion

    private void Awake()
    {
        bossCon = boss.GetComponent<BossController>();

        moveStack = new GridMoveStack();
        moveStack.AddDelegate(GridMoveType.Rotate, RotateGrid);
        RotateMoveComplete += moveStack.HandleMoveComplete;
        moveStack.AddDelegate(GridMoveType.AddSegment, AddSegment);
        AddSegmentMoveComplete += moveStack.HandleMoveComplete;
    }

    private void Start()
    {
        // In order to align correctly with the boss, this done because the BossGrid can not 
        // be a child of the boss, as the orientation is not directly linked with the boss
        transform.position = new Vector3(   boss.transform.position.x,
                                            boss.transform.position.y - bossCon.Settings.BossMeshBoundExtents.y*0.5f,
                                            boss.transform.position.z);
    }

    private void Update()
    {
        moveStack.Update();
    }

    /// <summary>
    ///  Prepare the grid layers for population, the layer and straight propertise are populated
    ///  for easy access to layer segment
    /// </summary>
    public void SetUpGrid()
    {
        // Figure out the max grid size from boss settings
        noOfLayers = (int)Mathf.Ceil((bossCon.Settings.DeathRadius - MinGridRadius) / LayerIncrement);

        bool allStrtsAdded = false;
        //TODO: Set up the segments, layer by layer here
        for (int layerCtr = -1; layerCtr < NoOfLayers; layerCtr++)
        {
            layers.Add(layerCtr, new Dictionary<int, BossGridSegment>());
            for (int straightCtr = 0; straightCtr < NoOfStraights; straightCtr++)
            {
                if (!allStrtsAdded) { straights.Add(straightCtr, new Dictionary<int, BossGridSegment>()); }
                BossGridSegment newSegment;
                SegmentType segmentType = layerCtr == -1 ? SegmentType.Generator : SegmentType.Standard;
                newSegment = new BossGridSegment
                (
                    this,
                    straightCtr,
                    layerCtr,
                    shieldSegmentPf,
                    segmentType
                );
                newSegment.SegmentShiftComplete  += HandleMoveComplete;
                newSegment.SegmentRotateComplete += HandleMoveComplete;
                layers[layerCtr].Add(newSegment.StraightNo, newSegment);
                straights[straightCtr].Add(newSegment.LayerNo, newSegment);
            }
            allStrtsAdded = true;
        }
    }
        
    public void PopulateGrid()
    {
        for (int layerCtr = 0; layerCtr < initialPopulatedLayers    ; layerCtr++)
        {
            foreach( KeyValuePair<int, BossGridSegment> kVp in layers[layerCtr])
            {
                BossGridSegment seg = kVp.Value;
                seg.Populate();
            }
        }
        UpdateGridMaxRadius();
        if (DebugOptions.GridReport) { Debug.Log(PrintGridStatus()); }
    }

    private void UpdateGridMaxRadius()
    {
        int maxLayerNo = 0;
        foreach (var kVP in Straights)
        {
            Dictionary<int, BossGridSegment> straight = kVP.Value;
            IEnumerable<BossGridSegment> populatedSegsInStraight = straight.Values.Where(o => o.Populated);
            int curMaxLayerNo = 0;
            if (populatedSegsInStraight.Count() > 0)
            {
             curMaxLayerNo = populatedSegsInStraight.Max(o => o.LayerNo);
            }
            maxLayerNo = curMaxLayerNo > maxLayerNo ? curMaxLayerNo : maxLayerNo;
        }
        GridMaxRadius = MinGridRadius + LayerIncrement * maxLayerNo;
    }

    public void AddMove(GridMove move)
    {
        moveStack.AddMove(move);
    }

    private void RotateGrid(GridMove move)
    {
        RotationMove rotMove = (RotationMove)move;
        string reportString = string.Empty;
        foreach (BossGridSegment seg in Layers[rotMove.LayerNo].Values)
        {
            if (seg.Populated) { move.SegsToMove.Add(seg); };
            seg.StartRotate(rotMove);
        }
        if (Layers[rotMove.LayerNo].Values.FirstOrDefault(o => o.Populated) != null)
        {
            reportString += "Rotating layer " + rotMove.LayerNo + " by " + rotMove.RotationDelta + "\n";
        }
        // Complete the move now if there is nothing to rotate
        if (move.SegsToMove.Count == 0) { move.Completed = true; }
        if (DebugOptions.GridReport) { Debug.Log(reportString); }
    }

    private void AddSegment(GridMove move)
    {
        AddSegmentMove addSegMove = (AddSegmentMove)move;
        // Generate a seg in the seg gen layer if there is space to shift it
        if (!Straights[addSegMove.StraightNo][NoOfLayers - 1].Populated)
        {
            Straights[addSegMove.StraightNo][-1].Populate();
            // Shift all the segs up
            foreach (BossGridSegment seg in Straights[addSegMove.StraightNo].Values)
            {
                if (seg.Populated) { move.SegsToMove.Add(seg); }
                seg.StartShift(addSegMove);
            }
            if (DebugOptions.GridReport) { Debug.Log("Adding seg at straight " + addSegMove.StraightNo);}
        }
    }

    private string PrintGridStatus()
    {
        string reportString = string.Empty;
        foreach (var keyVpL in Layers.Values)
        {
            Dictionary<int, BossGridSegment> layerSegList = keyVpL;
            if(layerSegList.Values.FirstOrDefault(o => o.Populated) == null) { continue; } 
            reportString += "L" + layerSegList[0].LayerNo.ToString("+00;-00");
            foreach (var seg in layerSegList.Values)
            {
                reportString += "[";
                //reportString += "S" + "(" + seg.LayerNo + "," + seg.StraightNo + ")";
                if (seg.Populated)
                {
                    reportString += seg.GetReportString(false);
                }
                else
                {
                    reportString += "C" + "(" + "N" + "," + "N" + ")";
                }
                reportString += "]";
            }
            reportString += "\n";
        }
        return reportString;
    }

    #region EventHandlers

    private void HandleMoveComplete(object sender, MoveCompleteEventArgs e)
    {
        var seg = (BossGridSegment)sender;
        e.Move.SegsMoved.Add((BossGridSegment)sender);
        // Update the list to make sure any segments that were destroyed
        // during the move are removed from consideration.
        e.Move.SegsToMove.RemoveAll(o => !o.Populated);
        if (e.Move.SegsToMove.All(e.Move.SegsMoved.Contains))
        {
            BossGridSegment.UpdateSegments(e.Move.SegsMoved, this);
            if(e.Move.Type == GridMoveType.AddSegment) { UpdateGridMaxRadius(); }
            // Reset tracking counters
            e.Move.SegsMoved.Clear();
            e.Move.SegsToMove.Clear();
            e.Move.Completed = true;
            RotateMoveComplete?.Invoke(this, e);
            if (DebugOptions.GridReport) { Debug.Log(PrintGridStatus()); }
        }
    }

    #endregion
}