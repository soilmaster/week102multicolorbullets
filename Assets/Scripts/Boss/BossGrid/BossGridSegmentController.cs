﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class SegmentLabel
{
    // Label
    [SerializeField]
    private GameObject labelGO;
    [SerializeField]
    private float labelHeight;
    [SerializeField]
    private float labelFontSize;
    private TextMeshPro labelTM;
    private BossGridSegmentController segCon;
    private bool initialized; 

    private bool active;
    public bool Active
    {
        get { return active; }
        set
        {
            if (initialized)
            {
                labelGO.SetActive(value);
                active = value;
            }
        }
    }

    public void Init(BossGridSegmentController inputSegCon)
    {
        segCon           = inputSegCon;
        labelTM          = labelGO.GetComponent<TextMeshPro>();
        labelTM.fontSize = labelFontSize;
        initialized      = true;
        Update();
    }


    public void Update()
    {
        if (!initialized) { return; }

        labelTM.text =
            "ID: " + segCon.GetInstanceID() + "\n" +
            "S: "  + segCon.StraightNo      + "\n" +
            "L: "  + segCon.LayerNo;
        labelTM.transform.localPosition = new Vector3(  segCon.MeshCenter.x,
                                                        segCon.MeshCenter.y + labelHeight,
                                                        segCon.MeshCenter.z);
        var mainCamera      = Camera.main;
        Vector3 difference  = labelTM.transform.position - mainCamera.transform.position;
        labelTM.transform.rotation = Quaternion.LookRotation(difference);
    }

    public void Clear()
    {
        labelGO = null;
        labelTM = null;
        initialized = false;
    }

}

public class BossGridSegmentController : MonoBehaviour
{
    #region Events

    public event System.EventHandler<MoveCompleteEventArgs> SegmentRotateComplete;
    public event System.EventHandler<MoveCompleteEventArgs> SegmentShiftComplete;
    public event System.EventHandler SegmentDestroyed;

    #endregion

    #region Properties

    public int StraightNo { get {return straightNo;} }
    private int straightNo;

    public int LayerNo { get{return layerNo;} }
    private int layerNo;

    public bool HasBeenHit { get { return hasBeenHit;} set { hasBeenHit = value;} }
    private bool hasBeenHit = false;

    public bool Initialized { get; set; }
  
    public Vector3 MeshCenter { get { return meshCenter; } set { meshCenter = value; } }
    private Vector3 meshCenter;

    #endregion

    #region Fields

    // Mesh
    [Range(0,100)]
    [SerializeField]
    private int  meshResolution;
    private float currentRadius;
    private float currentStartAngle;
    private float currentArcAngle;
    private Mesh mesh;
    private MeshCollider meshCollider;
    private ArcSegGenerator arcSegGen;

    // Colors
    private Color originalColor;
    private Material material;

    // Shift
    [SerializeField]
    private float shiftSpeed;
    private float startRadius;
    private float targetRadius;
    private float totalRadiusDelta;
    private int shiftDelta = 0;
    private float shiftElapsedTime = 0f;
    public bool IsShifting { get; set; }

    // Rotation
    [SerializeField]
    private float rotSpeed = 135.72f;
    private Quaternion startingRotation;
    private Quaternion targetRotation = Quaternion.identity;
    private float totalAngleDelta;
    private int rotDelta = 0;
    private float rotElapsedTime = 0f;
    public bool IsRotating { get; set; }


    // General
    [SerializeField]
    private SegmentLabel segLabel;
    [SerializeField]
    private GameObject shieldSegArcGO;
    private Rigidbody shieldCenterRigidBody;
    private int noOfLayers;
    private int noOfStraights;
    private GridMove currentMove;
    [SerializeField]
    private int health;
    #endregion

    void Awake()
    {
        material     = shieldSegArcGO.GetComponent<Renderer>().material;
        mesh         = shieldSegArcGO.GetComponent<MeshFilter>().mesh;
        meshCollider = shieldSegArcGO.GetComponent<MeshCollider>();
        arcSegGen    = new ArcSegGenerator();
            
        // Rotation
        shieldCenterRigidBody = GetComponent<Rigidbody>();
    }

    public void Init
    (
        Transform parentTransform,
        float gridSegAngle,
        float radius,
        float arcAngle,
        int inputStraightNo,
        int inputLayerNo,
        int inputNoOfLayers,
        int inputNoOfStraights
    )
    {
        // Set color
        material.color =
        GameManagmentProperties.ColorMan.GetRandomColorFromCat(GameColorCategory.Shield).Color;

        transform.parent    = parentTransform;
        currentRadius       = radius;
        currentStartAngle   = inputStraightNo * gridSegAngle;
        currentArcAngle     = arcAngle;
        noOfLayers          = inputNoOfLayers;
        noOfStraights       = inputNoOfStraights;
        UpdateMesh(currentRadius);
        UpdateGridLoc(inputLayerNo, inputStraightNo);
        segLabel.Init(this);
        Initialized = true;
    }

    void Update()
    {
        segLabel.Active = DebugOptions.GridSegmentLabels;
        if(DebugOptions.GridSegmentLabels)
        { 
            segLabel.Update();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateShift();
        UpdateRotate();
    }

    public void TakeDamage(int damage)
    {
        AudioManager.instance.Play("ShieldDamage");
        health -= damage;
        if(health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void UpdateGridLoc(int inputLayerNo, int inputStraightNo)
    {
        // Set the location within the grid
        straightNo   = inputStraightNo;
        layerNo      = inputLayerNo;
        name         = "Segment L" + layerNo + "S" + straightNo;
        segLabel.Update();
    }

    public void StartRotation(RotationMove rotMove, float segAngle)
    {
        rotDelta            = rotMove.RotationDelta;
        totalAngleDelta     = rotMove.RotationDelta * segAngle;
        startingRotation    = transform.rotation;
        targetRotation      = transform.rotation * Quaternion.Euler(new Vector3(0, totalAngleDelta, 0));
        IsRotating          = true;
        rotElapsedTime      = 0f;
        currentMove         = rotMove;
    }

    private void UpdateRotate()
    {       
        // Update the rotation
        if (IsRotating)
        {
            rotElapsedTime         += Time.fixedDeltaTime;
            float angleCoveredRatio = Mathf.Clamp((rotElapsedTime * rotSpeed) / totalAngleDelta, 0f, 1f);
            shieldCenterRigidBody.MoveRotation(Quaternion.Lerp(startingRotation, targetRotation, angleCoveredRatio));
            //TODO: clean up this comparison
            if (angleCoveredRatio >= 1f)
            {
                int offset = StraightNo - rotDelta;
                int newStraightNo = offset >= 0 ? offset : offset + noOfStraights;
                UpdateGridLoc(LayerNo, newStraightNo);
                IsRotating = false;
                rotDelta   = 0;
                SegmentRotateComplete?.Invoke(this, new MoveCompleteEventArgs
                {
                    Move = currentMove,
                });
                currentMove = null;
            }
        }
    }

    public void StartShift(AddSegmentMove addSegMove, float inputLayerIncrement)
    {
        shiftDelta       = addSegMove.ShiftDelta;
        totalRadiusDelta = inputLayerIncrement * addSegMove.ShiftDelta;
        startRadius      = currentRadius;
        targetRadius     = startRadius + totalRadiusDelta;
        IsShifting       = true;
        shiftElapsedTime = 0f;
        currentMove      = addSegMove;
    }

    private void UpdateShift()
    {
        // Update the shift
        if (IsShifting)
        {
            shiftElapsedTime     += Time.fixedDeltaTime;
            float completedRatio = Mathf.Clamp((shiftElapsedTime * shiftSpeed) / totalRadiusDelta, 0f, 1f);

            currentRadius = startRadius + completedRatio * totalRadiusDelta;

            UpdateMesh(currentRadius);

            //TODO: clean up this comparison
            if (completedRatio >= 1f)
            {
                UpdateGridLoc(LayerNo + shiftDelta, StraightNo);
                IsShifting = false;
                shiftDelta = 0;
                SegmentShiftComplete?.Invoke(this, new MoveCompleteEventArgs()
                {
                    Move = currentMove,
                });
                currentMove = null;
            }
        }
    }

    private void UpdateMesh(float radius)
    {
        // Set mesh for the shield object
        arcSegGen.Arc3DConfig = new Arc3DConfiguration
        {       
            StartAngle  = currentStartAngle,
            Width       = 0.5f,
            Height      = 0.5f,
            Center      = Vector3.zero,
            Radius      = radius,
            ArcAngle    = currentArcAngle,  
            Segments    = meshResolution
        };
        arcSegGen.Init(mesh);
        arcSegGen.UpdateMesh();

        meshCenter = arcSegGen.MeshCenter;

        // Set coollider
        meshCollider.sharedMesh  = mesh;
        meshCollider.convex      = true;
    }

    // Remove or add alpha based on whether damage was taken
    public void UpdateColor()
    {
        if(hasBeenHit)
        {
            AudioManager.instance.Play("ShieldDamage");
            material.color = new Color
            (
                originalColor.r, 
                originalColor.g, 
                originalColor.b, 
                0.5f
            );
        }
        else
        {
            material.color = originalColor;
        }
    }

    void OnDestroy()
    {
        segLabel.Clear();
        SegmentDestroyed?.Invoke(this, System.EventArgs.Empty);
    }


}
