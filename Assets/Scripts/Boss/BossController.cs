﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class BossSettings
{
    #region Properties

    public GameObject BossCore { get { return bossCore; } }
    [SerializeField]
    private GameObject bossCore;

    public int Health { get { return health; } set { health = value; } }
    [SerializeField]
    private int health;

    public GameObject PlayerShipAssembly { get { return playerShipAssembly;} }
    [SerializeField]
    private GameObject playerShipAssembly;
    
    public float DeathRadius { get { return deathRadius; } set { deathRadius = value; } }
    [SerializeField]
    private float deathRadius = 0f;

    public BossGridController BossGridCon { get { return bossGridCon; } }
    private BossGridController bossGridCon;

    public Transform BossTransform { get; set; }
    public Rigidbody BossRigidBody { get; set; }
    private Vector3 bossMeshBoundExtents;
    public Vector3 BossMeshBoundExtents { get { return bossMeshBoundExtents; } }   
    private RailController deathRailCon;
    public RailController DeathRailCon { get { return deathRailCon;} }

    #endregion

    #region Field
    
    [SerializeField]
    private GameObject bossGrid;
    [SerializeField]
    private GameObject deathRailPf;
        
    #endregion

    public void Init(GameObject inputBossGO)
    {
        BossTransform        = inputBossGO.transform;
        BossRigidBody        = inputBossGO.GetComponent<Rigidbody>();  
        bossGridCon          = bossGrid.GetComponent<BossGridController>();
        bossMeshBoundExtents = bossCore.GetComponent<MeshFilter>().mesh.bounds.extents;
        deathRailCon         = Object.Instantiate(deathRailPf,  BossTransform).GetComponent<RailController>();

        DeathRailCon.Init(BossTransform, deathRadius);
    }
}

public class BossController : MonoBehaviour
{
    #region Properties

    [SerializeField]
    private BossSettings settings;
    public BossSettings Settings { get{ return settings;} }


    [SerializeField]
    private BossAttackManager attackManager;
    public BossAttackManager AttackManager { get{ return attackManager;} }

    #endregion

    #region Fields

    private bool isAppQuitting;
    [SerializeField]
    private float addSegThreshold;
    private float addSegTimer = 0f;

    [SerializeField]
    private float invincibilityPeriod;
    private bool isVulnerable = true;
    private float invincibilityTimer = 0;
    private Color invincibleColor; 
    private Color standardColor;
    private Material bossCoreMat;
        
    #endregion

    void Awake()
    {
        //TODO: see if this needs consolidating
        attackManager.Init(settings);
        settings.Init(gameObject);
        settings.BossGridCon.RotateMoveComplete += HandleMoveGridLayersComplete;

        // Setting up the boss colors
        bossCoreMat = settings.BossCore.GetComponent<Renderer>().material;
        standardColor = bossCoreMat.color;
        ColorUtility.TryParseHtmlString("#6c05f2", out invincibleColor);
    }

    void Start()
    {
        attackManager.AttackType = BossAttackType.SimpleDirected;

        settings.BossGridCon.SetUpGrid();
        settings.BossGridCon.PopulateGrid();

        // Opening moves
        settings.BossGridCon.AddMove(new BatchRotateMove(new List<RotationMove> {
            new RotationMove(0, 1),
            new RotationMove(1, 1),
            new RotationMove(2, 1),
        }));
        settings.BossGridCon.AddMove(new AddSegmentMove(0, 1));
        settings.BossGridCon.AddMove(new BatchRotateMove(new List<RotationMove> {
            new RotationMove(0, 1),
            new RotationMove(1, 1),
            new RotationMove(2, 1),
        }));
        settings.BossGridCon.AddMove(new BatchAddSegmentMove(new List<AddSegmentMove> {
            new AddSegmentMove(0, 1),
            new AddSegmentMove(1, 1),
            new AddSegmentMove(2, 1),
        }));
    }   


    void Update()
    {
        settings.DeathRailCon.Radius = settings.DeathRadius;
        if (!isVulnerable)
        {
            if (invincibilityTimer >= invincibilityPeriod)
            {
                invincibilityTimer = 0;
                isVulnerable = true;
                bossCoreMat.color = standardColor;
            }
            invincibilityTimer += Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        attackManager.Attack();

        if(attackManager.ExpansionActive)
        {
            addSegTimer += Time.fixedDeltaTime;
            if (addSegTimer > addSegThreshold)
            {
                AudioManager.instance.Play("BossExpand");
                settings.BossGridCon.AddMove(new AddSegmentMove(Random.Range(0,settings.BossGridCon.NoOfStraights), 1));
                addSegTimer = 0;
            }
        }
    }

    public void TakeDamage()
    {
        if (isVulnerable)
        {
            settings.Health -= 1;
            AudioManager.instance.Play("BossDamage");
            if(settings.Health <= 0)
            {
                Destroy(this.gameObject);
            }
            if(settings.Health == 1)
            {
                attackManager.AttackType = BossAttackType.FanPattern;
                addSegThreshold = addSegThreshold * 0.7f;
            }
            isVulnerable = false;
            bossCoreMat.color = invincibleColor;
            BossGridShuffle();
        }
    }

    private void BossGridShuffle()
    {
        // Random shuffle for the whole grid
        List<RotationMove> randomRotMoves = new List<RotationMove>();
        for (int layerCtr = 0; layerCtr < settings.BossGridCon.NoOfLayers; layerCtr++)
        {
            randomRotMoves.Add(new RotationMove(layerCtr, Random.Range(0, settings.BossGridCon.NoOfStraights)));
        }
        settings.BossGridCon.AddMove(new BatchRotateMove(randomRotMoves));
    }

    private void HandleMoveGridLayersComplete(object sender, System.EventArgs e)
    {
    }

    void OnApplicationQuit()
    {
        isAppQuitting = true;
    }
    
    void OnDestroy()
    {
        if(!isAppQuitting)
        {
            AudioManager.instance.Play("BossDeath");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }
}
