﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BossProjectileController : MonoBehaviour
{
    [SerializeField]
    private float linearSpeed;
    private float arcSpeed;

    [SerializeField]
    private float killDistance;

    [SerializeField]
    private int damage;
    public Color CurrentColor;
    public ProjectileMovementType MovType { get; set; }

    private Transform bossTranform;
    private Transform shipTranform;
    private Vector3 bossLastKnowPos;

    public float ElevationAngle { get; set; }
    public float TargetAngle { get; set; }

    private float timeElapsedArcProj;

    void Awake()
    {
        MovType             = ProjectileMovementType.Arc;  
        timeElapsedArcProj  = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public void Init(
        Transform   inputbossTransform, 
        Transform   inputShipTranform, 
        float       elevationAngle, 
        float       targetAngle, 
        float       inputArcSpeed
    )
    {
        bossTranform        = inputbossTransform;
        shipTranform        = inputShipTranform;
        ElevationAngle      = elevationAngle;
        TargetAngle         = targetAngle;
        arcSpeed            = inputArcSpeed;
        Color fpProjColor   = GameManagmentProperties.ColorMan.GetAnyRandomColor().Color;
        gameObject.GetComponent<Renderer>().material.color = fpProjColor;
        gameObject.GetComponent<BossProjectileController>().CurrentColor = fpProjColor;
    }

    private void Update()
    {
        // Max distance of bullet
        if (Vector3.Distance(bossLastKnowPos, transform.position) > killDistance)
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (bossTranform != null)
        {
            bossLastKnowPos = bossTranform.position;
        }

        switch (MovType)
        {
            case ProjectileMovementType.Linear:
                transform.position += this.transform.forward * linearSpeed * Time.fixedDeltaTime;
                break;
            case ProjectileMovementType.Arc:
                timeElapsedArcProj += arcSpeed*Time.fixedDeltaTime;
                transform.position = UtilityFunctions.CalculateArcPosition
                (
                    bossLastKnowPos,
                    shipTranform.position, 
                    timeElapsedArcProj,
                    ElevationAngle,
                    TargetAngle
                );
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter (Collider other)
    {
        GameObject otherGO = other.gameObject;

        switch(otherGO.tag)
        {
            case "Boss":
                break;
            case "Shield":
                otherGO.GetComponentInParent<ShipController>().Absorb(CurrentColor);
                Destroy(this.gameObject);            
                break;
            case "Player":
                otherGO.GetComponentInParent<ShipController>().TakeDamage(damage);
                Destroy(this.gameObject);
                break;
            default:
                break;
        }


    }

}
