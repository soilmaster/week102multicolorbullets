using UnityEngine;

public enum ProjectileMovementType
{
    Linear,
    Arc
}

public enum BossAttackType
{
    SimpleDirected,
    FanPattern,
    ClusterPattern,
}

[System.Serializable]
public class PatternSettings
{
    [SerializeField]
    protected float projectilePeriod;
    public float ProjectilePeriod { get { return projectilePeriod; } set { projectilePeriod = value; } }

    [SerializeField]
    protected float projectileSpeed;
    public float ProjectileSpeed { get { return projectileSpeed; } set { projectileSpeed = value; } }
}

[System.Serializable]
public class SimpleDirectedSettings : PatternSettings
{
}

[System.Serializable]
public class FanPatternSettings : PatternSettings
{
    [SerializeField]
    private int fanPatEmitPnts;
    public int FanPatEmitPnts { get{return fanPatEmitPnts;} }

    [SerializeField]
    private Vector3 autoRotateRate;
    public Vector3 AutoRotateRate { get {return autoRotateRate;}}
}


[System.Serializable]
public class BossAttackManager
{

    #region Properties

    public float ProjectileTimer { get; set; }
    public BossAttackType AttackType { get; set; }    
    [SerializeField]
    private bool firingActive;
    public bool FiringActive { get { return firingActive; } set { firingActive = value; } } 
   
    [SerializeField]
    private bool expansionActive;
    public bool ExpansionActive { get { return expansionActive; } set { expansionActive = value; } }
    [SerializeField]
    private GameObject projectile;
    public GameObject Projectile { get { return projectile; } set { projectile = value; } }

    [Range(1f,89f)]
    [SerializeField]
    private float projectileElevationAngle;
    public float ProjectileElevationAngle { get { return projectileElevationAngle; } set { projectileElevationAngle = value; } }

    #endregion



    #region Field

    private BossSettings bossSettings;
    [SerializeField]
    private FanPatternSettings fanPatSettings;
    [SerializeField]
    private SimpleDirectedSettings simpleDirSettings;
        
    #endregion


    public void Init(BossSettings inputBossSettings)
    {
        bossSettings    = inputBossSettings;
        ProjectileTimer = 0;
        AttackType      = BossAttackType.SimpleDirected;
    }

    public void Attack()
    {
        // Update the bosses position/rotation before the attack
        // based upon the type of attack that is currently configured
        switch(AttackType)
        {
            case BossAttackType.SimpleDirected:
                // Rotate to face player ship
                if(bossSettings.PlayerShipAssembly != null)
                {
                    bossSettings.BossTransform.LookAt(bossSettings.PlayerShipAssembly.transform, Vector3.up);
                }
                if(ProjectileTimer > simpleDirSettings.ProjectilePeriod)
                {
                    Fire();
                    ProjectileTimer = 0;
                }                break;
            case BossAttackType.FanPattern:
                bossSettings.BossRigidBody.MoveRotation(bossSettings.BossTransform.rotation*
                                                        Quaternion.Euler(fanPatSettings.AutoRotateRate));
                if(ProjectileTimer > fanPatSettings.ProjectilePeriod)
                {
                    Fire();
                    ProjectileTimer = 0;
                }
                break;
            default:
                break;
        }

        ProjectileTimer += Time.fixedDeltaTime;
    }

    private void Fire()
    {
        // Bail out if attacks have been disabled
        if(!FiringActive){return;}

        switch (AttackType)
        {
            case BossAttackType.SimpleDirected:
                AudioManager.instance.Play("BossFire");
                GameObject proj =  GameObject.Instantiate ( Projectile, 
                                                            bossSettings.BossTransform.position, 
                                                            bossSettings.BossTransform.rotation);
                proj.GetComponent<BossProjectileController>().Init
                (
                    bossSettings.BossTransform, 
                    bossSettings.PlayerShipAssembly.transform,
                    ProjectileElevationAngle,
                    bossSettings.BossTransform.rotation.eulerAngles.y,
                    simpleDirSettings.ProjectileSpeed
                );
                break; 
            case BossAttackType.FanPattern:
                Quaternion fpProjRot = Quaternion.Euler(0, 360/fanPatSettings.FanPatEmitPnts,0);
                AudioManager.instance.Play("BossFire");
                for (int projCtr = 0; projCtr < fanPatSettings.FanPatEmitPnts; projCtr++)
                {
                    bossSettings.BossTransform.rotation = bossSettings.BossTransform.rotation*fpProjRot;
                    GameObject fpProjGO =  GameObject.Instantiate ( Projectile, 
                                                                    bossSettings.BossTransform.position, 
                                                                    bossSettings.BossTransform.rotation);
                    fpProjGO.GetComponent<BossProjectileController>().Init
                    (
                        bossSettings.BossTransform, 
                        bossSettings.PlayerShipAssembly.transform,
                        ProjectileElevationAngle,
                        bossSettings.BossTransform.rotation.eulerAngles.y,
                        fanPatSettings.ProjectileSpeed
                    );
                }
                break;
            case BossAttackType.ClusterPattern:
                break;
            default:
                break;        
        }

        
    }

    
}