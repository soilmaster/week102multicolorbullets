﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpGeneratorScript : MonoBehaviour
{
    [SerializeField]
    private GameObject pickUpSegment;

    [SerializeField]
    [RangeAttribute(0, 360)]
    private float healthTargetSize;

    [SerializeField]
    private float radius;

    [SerializeField]
    private float rotationRate;

    [SerializeField]
    private GameObject healthPickUp;

    [SerializeField]
    private GameObject shipAssembly;

    private ArcSegGenerator arcSegMeshGen;

    private Vector3 pickupSpawnPoint;




    // Start is called before the first frame update
    void Start()
    {
        arcSegMeshGen = new ArcSegGenerator();
        GeneratePowerUpSegment();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        this.transform.Rotate(new Vector3(0,rotationRate,0));
    }



    private void GeneratePowerUpSegment()
    {
        // Create the shield object which needs to 
        GameObject pickupSegment = Instantiate(pickUpSegment, transform);
        // Set color
        pickupSegment.GetComponent<Renderer>().material.color = GameManagmentProperties.ColorMan.GetColor("HealthPickUp").Color;
        // Set mesh for the shield object
        Mesh shieldMesh = pickupSegment.GetComponent<MeshFilter>().mesh;
        arcSegMeshGen.Arc3DConfig = new Arc3DConfiguration
        {       
            StartAngle  = 0,
            Width       = 0.5f,
            Height      = 0.5f,
            Center      = Vector3.zero,
            Radius      = radius,
            ArcAngle    = healthTargetSize,  
            Segments    = 10
        };
        arcSegMeshGen.Init(shieldMesh);
        arcSegMeshGen.UpdateMesh();
        
        PickUpSegmentController pickUpSegmentController = pickupSegment.GetComponent<PickUpSegmentController>();
        pickUpSegmentController.ArcMeshCenter = arcSegMeshGen.MeshCenter;
        pickUpSegmentController.ShipPathRadius = Mathf.Abs(shipAssembly.transform.localPosition.y);

        // Set coollider
        pickupSegment.GetComponent<MeshCollider>().sharedMesh = shieldMesh;
       
        // Active the relevant color
        GameManagmentProperties.ColorMan.GetColor("HealthPickUp").Active = true;
    }
}
