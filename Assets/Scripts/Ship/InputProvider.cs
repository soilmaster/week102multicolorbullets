using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using UnityEngine;

public enum TouchButtonPhase
{
    Started,
    Ended,
}

public class InputProvider : MonoBehaviour
{

    #region Properties

    public bool LeftStarted     { get { return GetDirectionStarted(true); } }
    public bool LeftFinished    { get { return GetDirectionFinished(true); } }
    public bool LeftPressed     { get { return GetDirectionPressed(true); } }

    public bool RightStarted    { get { return GetDirectionStarted(false); } }
    public bool RightFinished   { get { return GetDirectionFinished(false); } }
    public bool RightPressed    { get { return GetDirectionPressed(false); } }

    public bool FireStarted     { get { return GetFireButtonStarted(); } }
    public bool FireFinished    { get { return GetFireButtonFinished(); } }
    public bool ShieldStarted   { get { return GetShieldButtonStarted(); } }
    public bool ShieldFinished  { get { return GetShieldButtonFinished(); } }

    #endregion

    #region Field

    // These two values determine the split between four buttons on screen
    private int screenCenterX;
    private int screenCenterY;
    // Required for the fact that we have two buttons for the same action
    private bool shieldTouchButtonEngaged;
    private TouchButtonPhase shieldTouchButtonPhase;
    private bool fireTouchButtonEngaged;
    private TouchButtonPhase fireTouchButtonPhase;

    #endregion


    public void Awake()
    {
        screenCenterX = Screen.width/2;
        screenCenterY = Screen.height/3;
    }

    public void ShieldTouchButtonStart()
    {
        if (!shieldTouchButtonEngaged)
        {
            shieldTouchButtonEngaged = true;
            shieldTouchButtonPhase  = TouchButtonPhase.Started;
            Debug.Log("Sheild button pressed");
        }
    }

    public void ShieldTouchButtonEnd()
    {
        if(shieldTouchButtonEngaged)
        {
            shieldTouchButtonEngaged = false;
            shieldTouchButtonPhase = TouchButtonPhase.Ended;
            Debug.Log("Sheild button released");
        }
    }

    public void FireTouchButtonStart()
    {
        if (!fireTouchButtonEngaged)
        {
            fireTouchButtonEngaged = true;
            fireTouchButtonPhase = TouchButtonPhase.Started;
            Debug.Log("Fire button pressed");
        }
    }

    public void FireTouchButtonEnd()
    {
        if (fireTouchButtonEngaged)
        {
            fireTouchButtonEngaged = false;
            fireTouchButtonPhase = TouchButtonPhase.Ended;
            Debug.Log("Fire button released");
        }
    }

    private bool GetDirectionStarted(bool leftDirection)
    {
        // Default response if the touch buttons are being pressed
        if(shieldTouchButtonEngaged || fireTouchButtonEngaged ) { return false; }
        // Touch logic
        bool touchResponse = false;
        if(Input.touchCount > 0)
        {
            Touch currentTouch = Input.GetTouch(0);
            if (currentTouch.phase == TouchPhase.Began)
            {
                if(leftDirection && currentTouch.position.x < screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
                else if (!leftDirection && currentTouch.position.x > screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
            }
        }
        // Keyboard logic
        bool keyResponse   = false;
        if (leftDirection)
        {
            keyResponse = Input.GetKeyDown(KeyCode.LeftArrow);
        }
        else
        {
            keyResponse = Input.GetKeyDown(KeyCode.RightArrow);
        }
        return keyResponse || touchResponse;
    }

    private bool GetDirectionFinished(bool leftDirection)
    {
        // Default response if the touch buttons are being pressed
        if (shieldTouchButtonEngaged || fireTouchButtonEngaged) { return false; }
        // Touch logic
        bool touchResponse = false;
        if(Input.touchCount > 0)
        {
            Touch currentTouch = Input.GetTouch(0);
            if (currentTouch.phase == TouchPhase.Ended)
            {
                if(leftDirection && currentTouch.position.x < screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
                else if (!leftDirection && currentTouch.position.x > screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
            }
        }
        // Keyboard logic
        bool keyResponse   = false;
        if (leftDirection)
        {
            keyResponse = Input.GetKeyUp(KeyCode.LeftArrow);
        }
        else
        {
            keyResponse = Input.GetKeyUp(KeyCode.RightArrow);
        }
        return keyResponse || touchResponse;
    }

    private bool GetDirectionPressed(bool leftDirection)
    {
        // Default response if the touch buttons are being pressed
        if (shieldTouchButtonEngaged || fireTouchButtonEngaged) { return false; }
        // Touch logic
        bool touchResponse = false;
        if(Input.touchCount > 0)
        {
            Touch currentTouch = Input.GetTouch(0);
            if  (
                    currentTouch.phase == TouchPhase.Moved ||
                    currentTouch.phase == TouchPhase.Stationary 
                )
            {
                if(leftDirection && currentTouch.position.x < screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
                else if (!leftDirection && currentTouch.position.x > screenCenterX && currentTouch.position.y > screenCenterY)
                {
                    touchResponse = true; 
                }
            }
        }
        // Keyboard logic
        bool keyResponse   = false;
        if (leftDirection)
        {
            keyResponse = Input.GetKey(KeyCode.LeftArrow);
        }
        else
        {
            keyResponse = Input.GetKey(KeyCode.RightArrow);
        }
        return keyResponse || touchResponse;
    }

    private bool GetShieldButtonStarted()
    {
        bool touchResponse  = shieldTouchButtonEngaged && shieldTouchButtonPhase == TouchButtonPhase.Started ? true : false;
        bool keyResponse    = false;
        keyResponse         = Input.GetButtonDown("Fire2");
        return keyResponse || touchResponse;
    }

    private bool GetShieldButtonFinished()
    {
        bool touchResponse = !shieldTouchButtonEngaged && shieldTouchButtonPhase == TouchButtonPhase.Ended ? true : false;
        bool keyResponse = false;
        keyResponse = Input.GetButtonUp("Fire2");
        return keyResponse || touchResponse;
    }

    private bool GetFireButtonStarted()
    {
        bool touchResponse = fireTouchButtonEngaged && fireTouchButtonPhase == TouchButtonPhase.Started ? true : false;
        bool keyResponse = false;
        keyResponse = Input.GetButtonDown("Fire1");
        return keyResponse || touchResponse;
    }

    private bool GetFireButtonFinished()
    {
        bool touchResponse = !fireTouchButtonEngaged && fireTouchButtonPhase == TouchButtonPhase.Ended ? true : false;
        bool keyResponse = false;
        keyResponse = Input.GetButtonUp("Fire1");
        return keyResponse || touchResponse;
    }

    public void HapticVibrateStart()
    {
        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void HapticVibrateFinish()
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

}
