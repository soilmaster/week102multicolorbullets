﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


[System.Serializable]
public class ShieldSettings
{
    #region Properties
    public bool ParryOccured { get; set; }
        
    #endregion

    #region Fields

    [SerializeField]
    private GameObject shieldGO;
    private InputProvider inputSource;

    [SerializeField]
    private float shieldPeriod;
    private float sheildPeriodTimer;
    [SerializeField]
    private float shieldClDwnPrd;
    private float shieldClDwnPrdTimer;
    private bool sheildUsed;
    private InputProvider inputProvider;
        
    #endregion

    public void Init(InputProvider newInputProvider)
    {
        shieldGO.SetActive(false);

        sheildPeriodTimer   = shieldPeriod;
        shieldClDwnPrdTimer = shieldClDwnPrd;
        sheildUsed          = false;
        inputSource         = newInputProvider;

        ParryOccured        = false;
    }

    public void UdpdateShield()
    {
        // Reset cooldown if succesful parry occured
        if(ParryOccured)
        {
            sheildUsed          = false;
            shieldClDwnPrdTimer = 0;
            shieldClDwnPrdTimer = shieldClDwnPrd;
            ParryOccured        = false;
        }

        // Activate shield
        if(inputSource.ShieldStarted && shieldClDwnPrdTimer == shieldClDwnPrd)
        {
            shieldGO.SetActive(true);
            sheildUsed = true;
            AudioManager.instance.Play("ShieldUp");
            inputSource.HapticVibrateStart();
        }

        // Check if the shield active past the shield period
        if(shieldGO.activeInHierarchy)
        {
            sheildPeriodTimer -= Time.fixedDeltaTime;

            if(sheildPeriodTimer <= 0)
            {
                sheildPeriodTimer = shieldPeriod;
                shieldGO.SetActive(false);
            }
        }

        // Update the timer for the shield cooldown
        if (sheildUsed)
        {
            shieldClDwnPrdTimer -= Time.fixedDeltaTime;

            if(shieldClDwnPrdTimer <= 0)
            {
                sheildUsed = false;
                shieldClDwnPrdTimer = shieldClDwnPrd;
            }
        }

        if(inputSource.FireFinished)
        {
            inputSource.HapticVibrateFinish();
        }

    }
}

public class ShipController : MonoBehaviour
{

    #region Properties
    [SerializeField]
    private CircleMovement movement;
    public CircleMovement Movement  { get { return movement; } }

    [SerializeField]
    private ShieldSettings shield;
    public ShieldSettings Shield  { get { return shield; } }

    public float CurrentRadius { get { return distFromBoss + bossCon.Settings.BossGridCon.GridMaxRadius; } }

    #endregion

    #region Fields

    // Game objects
    [SerializeField]
    private GameObject boss;
    [SerializeField]
    private GameObject shipAssembly;
    [SerializeField]
    private GameObject shipGun;
    [SerializeField]
    private GameObject inputProviderGO;

    // Prefabs
    [SerializeField]
    private GameObject projectilePf; 
    [SerializeField]
    private GameObject shipRailPf;
    [SerializeField]
    private int health;
    [SerializeField]
    private Text healthText;

    // Color tank
    [SerializeField]
    private GameObject chargeTank1;
    [SerializeField]
    private GameObject chargeTank2;
    [SerializeField]
    private GameObject chargeTank3;
    private List<Color> chargedColors;

    // General
    private BossController bossCon;
    private RailController shipRailCon;
    private bool isAppQuitting;
    private int chargeLevel = 0;
    private int colorId     = 0;
    private InputProvider inputProvider;
    [SerializeField]
    private float distFromBoss = 2f;

    // Fire
    [SerializeField]
    private float fireCoolDown;
    private float fireTimer     = 0;
    private bool canFire        = true;
    private bool firePressed    = false;

    #endregion

    void Awake()
    {
        chargedColors       = new List<Color>();
        healthText.text     = "Health: " + health; 
        inputProvider       = inputProviderGO.GetComponent<InputProvider>();
        transform.position  = boss.transform.position;
        bossCon             = boss.GetComponent<BossController>();

        movement.Init(gameObject, shipAssembly.transform, inputProvider);
        shield.Init(inputProvider);

        //TODO: This may need to move to a GameManager object when there is one
        MMVibrationManager.iOSInitializeHaptics ();
    }

    // Start is called before the first frame update 
    void Start()
    {
        shipRailCon = Instantiate(shipRailPf, transform).GetComponent<RailController>();
        shipRailCon.Init(boss.transform, movement.Radius);
    }

    void Update()
    {
        //Movement.Move();
        Movement.Radius = shipRailCon.Radius = distFromBoss + bossCon.Settings.BossGridCon.GridMaxRadius;
        UpdateFireStatus();
    }

    void FixedUpdate()
    {
        Shield.UdpdateShield();
    }

    private void UpdateFireStatus()
    {
        if (!canFire)
        {
            if (fireTimer >= fireCoolDown)
            {
                fireTimer = 0;
                canFire = true;
            }
            fireTimer += Time.deltaTime;
        }
        if (inputProvider.FireStarted)
        {
            firePressed = true;
            inputProvider.HapticVibrateStart();
        }
        if (inputProvider.FireFinished)
        {
            firePressed = false;
            inputProvider.HapticVibrateFinish();
        }
        if (firePressed) { Fire(); }
        // Death rail check
        if (movement.Radius >= bossCon.Settings.DeathRadius)
        {
            Destroy(gameObject);
        }
    }
    
    private void Fire()
    {
        if (canFire)
        {
            if(chargeLevel > 0)
            {
                AudioManager.instance.Play("ShipFireStandard");
                GameObject proj = Instantiate(projectilePf, shipAssembly.transform.position, shipAssembly.transform.rotation);
                proj.GetComponent<ProjectileController>().AssembleStandardShot(chargeTank1.GetComponent<Renderer>().material.color);
            }
            //if(chargeLevel > 0)
            //{
            //    AudioManager.instance.Play("ShipFire");
            //    GameObject proj = Instantiate(projectilePf, shipAssembly.transform.position, shipAssembly.transform.rotation);
            //    proj.GetComponent<ProjectileController>().AssembleChargedShot(chargedColors);
            //    ResetTanks();
            //}
            canFire = false;
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        healthText.text = "Health: " + health; 
        AudioManager.instance.Play("ShipDamage");
        if(health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void TakeHealth(int healthAmount)
    {
        // TODO: Change the way this is done, maybe event
        health += healthAmount;
        healthText.text = "Health: " + health; 
    }

    public void Absorb(Color inputColor)
    {
        switch(chargeLevel)
        {
            case 0:
                chargeTank1.GetComponent<Renderer>().material.color = inputColor;
                chargedColors.Add(inputColor);
                chargeLevel++;
                AudioManager.instance.Play("ShieldAbsorb");
                break;
            //case 1:
            //    chargeTank2.GetComponent<Renderer>().material.color = inputColor;
            //    chargedColors.Add(inputColor);
            //    chargeLevel++;            
            //    AudioManager.instance.Play("ShieldAbsorb");
            //    break;
            //case 2:
            //    chargeTank3.GetComponent<Renderer>().material.color = inputColor;
            //    chargedColors.Add(inputColor);
            //    chargeLevel++;
            //    AudioManager.instance.Play("ShieldAbsorb");
            //    break;
            default:
                goto case 0;

        }

        shield.ParryOccured = true;
    }

    private void ResetTanks()
    {
        chargeTank1.GetComponent<Renderer>().material.color = Color.white;
        chargeTank2.GetComponent<Renderer>().material.color = Color.white;
        chargeTank3.GetComponent<Renderer>().material.color = Color.white;
        chargeLevel = 0;
        chargedColors.Clear();
    }
    
    void OnApplicationQuit()
    {
        isAppQuitting = true;
    }
                
    void OnDestroy()
    {
        if(!isAppQuitting)
        {
            AudioManager.instance?.Play("ShipDeath");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }

    void OnDisable()
    {
        MMVibrationManager.iOSReleaseHaptics();
    }

}
