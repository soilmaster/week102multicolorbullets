﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileController : MonoBehaviour
{
    [SerializeField]
    private int damage;
    public int Damage { get { return damage; } private set {damage = value;} }

    #region Fields

    [SerializeField]
    private float speed;
    [SerializeField]
    private GameObject projectileSegmentPf;
    [SerializeField]
    private float projSegmentSeperation;

    private Vector3 projSegLocScale;
    private Vector3 projSegmentExtents;
    private int chargedShots = 0;
    private bool markedForDestruction;
    private Dictionary<int, GameObject> impactedShieldSegs = new Dictionary<int, GameObject>();

    #endregion


    void Awake()
    {
        foreach (Transform child in transform)
        {
            chargedShots++;
        }

        projSegmentExtents = projectileSegmentPf.GetComponent<Renderer>().bounds.extents;
    }

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 3);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.gameObject.transform.position += this.transform.up * speed * Time.fixedDeltaTime;
    }

    //TODO: should call AssembleStandardShot multiple times
    public void AssembleChargedShot(List<Color> inputColors)
    {  
        for ( int colorCount = 1; colorCount <= inputColors.Count; colorCount++)
        {
            GameObject segment = Instantiate 
            (
                projectileSegmentPf, 
                this.gameObject.transform.position, 
                this.gameObject.transform.rotation, 
                this.gameObject.transform
            );
            Vector3 segPos = new Vector3    
            (
                segment.transform.localPosition.x,
                segment.transform.localPosition.y - colorCount*(projSegmentExtents.y*2 + projSegmentSeperation),
                segment.transform.localPosition.z
            );
            segment.transform.localPosition = segPos;
            segment.GetComponent<Renderer>().material.color = inputColors[colorCount-1];
        }
    }

    public void AssembleStandardShot(Color inputColor)
    {
        GameObject segment = Instantiate
        (
            projectileSegmentPf,
            this.gameObject.transform.position,
            this.gameObject.transform.rotation,
            this.gameObject.transform
        );
        Vector3 segPos = new Vector3
        (
            segment.transform.localPosition.x,
            segment.transform.localPosition.y,
            segment.transform.localPosition.z
        );
        segment.transform.localPosition = segPos;
        segment.GetComponent<Renderer>().material.color = inputColor;
    }


    void OnDestroy()
    {
        if(!markedForDestruction)
        {
            //Return shield to its original state
            foreach (GameObject gO in impactedShieldSegs.Values)
            {
                BossGridSegmentController segmentCon = gO.GetComponent<BossGridSegmentController>();
                segmentCon.HasBeenHit = false;
                segmentCon.UpdateColor();
            }
        }
    }
}
