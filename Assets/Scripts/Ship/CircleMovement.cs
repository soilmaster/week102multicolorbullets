using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CircleMovementAccelerationDirection
{
    None,
    Clockwise,
    Counterclockwise,
}

public class CircleMovementState
{
    public float AngAcc     { get; set; }
    public float AngVel     { get; set; }
    public float MaxAngVel  { get; set; }
    public Quaternion Rot   { get; set; } 
    public bool IsBeingStopped  { get; set; }
    public bool IsAtMaxAngVel   { get; set; }
    public CircleMovementAccelerationDirection AccDir { get; set; }

    public CircleMovementState()
    {
        AngAcc          = 0f;
        AngVel          = 0f;
        MaxAngVel       = 0f;
        Rot             = Quaternion.identity;
        IsBeingStopped  = false;
        IsAtMaxAngVel   = false;
        AccDir = CircleMovementAccelerationDirection.None;
    }

    public CircleMovementState(CircleMovementState stateToCopy)
    {
        AngAcc          = stateToCopy.AngAcc;
        AngVel          = stateToCopy.AngVel;
        MaxAngVel       = stateToCopy.MaxAngVel;
        Rot             = stateToCopy.Rot;
        IsBeingStopped  = stateToCopy.IsBeingStopped;
        IsAtMaxAngVel   = stateToCopy.IsAtMaxAngVel;
        AccDir          = stateToCopy.AccDir;
    }

    public void Stop()
    { 
        AngAcc = 0;
        AngVel = 0;
        IsBeingStopped = false;
        AccDir = CircleMovementAccelerationDirection.None;
    }

}

/// <summary>
/// A class that allows for smooth accelerated movement on a 2D XZ plane aligned circle 
/// </summary>
[System.Serializable]
public class CircleMovement
{
    #region Properties

    public float Radius { get { return radius; } set { radius = value; } }
    private float radius;


    #endregion

    #region Fields


    // Acceleration
    [SerializeField]
    private float angularAcc;
    
    // Deceleration
    [SerializeField]
    private float angularDec;

    // Speed
    [SerializeField]
    private float maxAngVel;

    // General
    private Transform CenterTransform;
    private Transform AssemblyTransform;
    private InputProvider inputProvider;

    [SerializeField]
    private bool isInputRightActive;
    [SerializeField]
    private bool isInputLeftActive;

    private CircleMovementState circleMoveState;

    #endregion


    public void Init(GameObject movementCenter, Transform inputMovementAssembly, InputProvider inpProvider)
    {
        CenterTransform   = movementCenter.transform;
        AssemblyTransform = inputMovementAssembly;
        inputProvider     = inpProvider;
        circleMoveState   = new CircleMovementState();
    }

    /// <summary>
    /// Should be run from Update()
    /// </summary>
    public void UpdateInput()
    {
        // Input starts
        if (inputProvider.RightStarted || inputProvider.LeftStarted)
        {
            inputProvider.HapticVibrateStart();
        }


        // Input ends
        if (inputProvider.RightFinished || inputProvider.LeftFinished)
        {
            inputProvider.HapticVibrateFinish();
        }
    }

    /// <summary>
    /// Should be run from FixedUpdate()
    /// </summary>
    public void UpdateMovement()
    {
        UpdateMovementAssembly();

        circleMoveState.MaxAngVel = maxAngVel;

        // Input continues
        isInputLeftActive  = inputProvider.LeftPressed;
        isInputRightActive = inputProvider.RightPressed;
        //isInputRightActive = true;

        // Figure out the acceleration to use
        if (isInputLeftActive && !isInputRightActive)
        {
            circleMoveState.IsBeingStopped = false;
            circleMoveState.AccDir = CircleMovementAccelerationDirection.Clockwise;
            circleMoveState.AngAcc = circleMoveState.IsAtMaxAngVel ? 0 : angularAcc;
            //Debug.Log("left active, right inactive" );
        }
        else if (isInputRightActive && !isInputLeftActive)
        {
            circleMoveState.IsBeingStopped = false;
            circleMoveState.AccDir = CircleMovementAccelerationDirection.Counterclockwise;
            circleMoveState.AngAcc = circleMoveState.IsAtMaxAngVel ? 0 : -angularAcc;
            //Debug.Log("right active, left inactive");
        }
        else
        {
            circleMoveState.IsBeingStopped = true;
            circleMoveState.AngAcc = circleMoveState.AngVel > 0 ? -angularDec : angularDec;
        }

        UpdateCircleMovementState(circleMoveState);

        CenterTransform.rotation = circleMoveState.Rot;
    }

    public CircleMovementState GetCircleMovementStateCopy()
    {
        return new CircleMovementState(circleMoveState);
    }


    private int bugCount = 0;

    public void UpdateCircleMovementState(CircleMovementState circMovState)
    {
        float deltaAngVel = Time.fixedDeltaTime * circMovState.AngAcc;

        // Check to see if the next acceleration update will change the velocity sign
        if (circMovState.IsBeingStopped)
        {
            circMovState.IsAtMaxAngVel = false;
            if (Mathf.Abs(circMovState.AngVel) < Mathf.Abs(deltaAngVel))
            {
                circMovState.Stop();
                return;
            }
        }

        if (!circMovState.IsBeingStopped)
        {
            if (!circMovState.IsAtMaxAngVel)
            {
                float deltaAngVelToMax = circMovState.MaxAngVel - Mathf.Abs(circMovState.AngVel);
                if (deltaAngVelToMax < Mathf.Abs(deltaAngVel))
                {
                    circleMoveState.AngAcc = 0f;
                    if (circleMoveState.AccDir == CircleMovementAccelerationDirection.Clockwise)
                    { circleMoveState.AngVel = circleMoveState.MaxAngVel; }
                    else if (circleMoveState.AccDir == CircleMovementAccelerationDirection.Counterclockwise)
                    { circleMoveState.AngVel = -circleMoveState.MaxAngVel; }
                    circleMoveState.IsAtMaxAngVel = true;
                }
            }
            else if (circMovState.IsAtMaxAngVel)
            {

            }
        }

        // Check to see if the next acceleration update will push angVel over maxAngVel

        if (isInputLeftActive && circMovState.AngVel == -circMovState.MaxAngVel)
        {
            bugCount++;
        }
        if (isInputRightActive && circMovState.AngVel == circMovState.MaxAngVel)
        {
            bugCount++;
        }

        // Update the angle based on SUVAT equation for angular velocity/acc
        float deltaAngle = circMovState.AngVel * Time.fixedDeltaTime +
                           0.5f * circMovState.AngAcc * Time.fixedDeltaTime * Time.fixedDeltaTime;

        circMovState.AngVel += deltaAngVel;

        circMovState.Rot *= Quaternion.Euler(0, deltaAngle * Mathf.Rad2Deg, 0);
    }



    private void UpdateMovementAssembly()
    {
        // Update distance from the center
        AssemblyTransform.transform.localPosition =
        new Vector3(
            -Radius,
            AssemblyTransform.transform.localPosition.y,
            AssemblyTransform.transform.localPosition.z
        );
    }

}
