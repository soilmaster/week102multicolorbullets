﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipProjSegController : MonoBehaviour
{
    private ProjectileController parentProjectileCon;

    void Awake()
    {
        parentProjectileCon = this.GetComponentInParent<ProjectileController>();
    }

    //TODO: Clean up on common code
    private void OnTriggerEnter (Collider other)
    {
        GameObject otherGO = other.gameObject;

        switch (otherGO.tag)
        {
            case "Player":
                break;
            case "Shield":
                break;
            case "Boss":
                otherGO.GetComponentInParent<BossController>().TakeDamage();
                Destroy(this.gameObject);
                break;
            case "PickUpSegment":
                if(         otherGO.GetComponent<Renderer>().material.color == 
                    this.gameObject.GetComponent<Renderer>().material.color)
                {
                    Destroy(otherGO);
                    Destroy(this.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                }
                break;
            case "BossShield":
                if(         otherGO.GetComponent<Renderer>().material.color == 
                    this.gameObject.GetComponent<Renderer>().material.color)
                {
                    otherGO.GetComponentInParent<BossGridSegmentController>().TakeDamage(parentProjectileCon.Damage);
                    Destroy(this.gameObject);
                }
                else
                {
                    Destroy(this.gameObject);
                }
                break;
            default:
                Destroy(this.gameObject);
                break;
        }
    }

    void OnDestroy()
    {
    }
}
