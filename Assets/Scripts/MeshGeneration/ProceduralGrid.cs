﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class ProceduralGrid : MonoBehaviour
{
    //Mesh variables
    Mesh mesh;
    Vector3[] vertices;
    int[] triangles;


    // Grid settings 
    public float cellSize = 1;
    public Vector3 gridOffset;
    public int gridSize = 1;


    // Start is called before the first frame update
    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Start()
    {
        // MakeProdecuralDiscreteGrid();
        MakeProdecuralContiguousGrid();
        UpdateMesh();
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

    }


    private void MakeProdecuralContiguousGrid()
    {
        vertices  = new Vector3[(gridSize + 1)*(gridSize + 1)];
        triangles = new int[gridSize*gridSize*2*3];

        float halfCellSize = cellSize * 0.5f;

        int vertQdShift = 0;
        int triQdShift  = 0;


        // Setting the vertices
        for(int xIndex = 0; xIndex <= gridSize; xIndex++)
        {
            for(int zIndex = 0; zIndex <= gridSize; zIndex++)
            {   

                Vector3 quadOffset = new Vector3(cellSize * xIndex, 0, cellSize * zIndex);

                vertices[vertQdShift] = new Vector3((cellSize*xIndex - halfCellSize), 0 , (cellSize*zIndex - halfCellSize)) + gridOffset;

                vertQdShift++;
            }   
        } 

        vertQdShift = 0;
        
        for(int xIndex = 0; xIndex < gridSize; xIndex++)
        {
            for(int zIndex = 0; zIndex < gridSize; zIndex++)
            {   
                triangles[triQdShift + 0] = vertQdShift + 0;
                triangles[triQdShift + 1] = vertQdShift + 1;
                triangles[triQdShift + 2] = vertQdShift + gridSize + 1;
                triangles[triQdShift + 3] = vertQdShift + gridSize + 1;
                triangles[triQdShift + 4] = vertQdShift + 1;
                triangles[triQdShift + 5] = vertQdShift + gridSize + 1 + 1;
                
                vertQdShift++;
                triQdShift += 6;
            }
            vertQdShift++;
        }


    }

    private void MakeProdecuralDiscreteGrid()
    {
        vertices  = new Vector3[gridSize*gridSize*4];
        triangles = new int[gridSize*gridSize*2*3];

        float halfCellSize = cellSize * 0.5f;

        int vertQdShift = 0;
        int triQdShift  = 0;

        for(int xIndex = 0; xIndex < gridSize; xIndex++)
        {
            for(int zIndex = 0; zIndex < gridSize; zIndex++)
            {   
                Vector3 quadOffset = new Vector3(cellSize * xIndex, 0, cellSize * zIndex);


                vertices[vertQdShift + 0] = new Vector3(-halfCellSize, 0 , -halfCellSize) + gridOffset + quadOffset;
                vertices[vertQdShift + 1] = new Vector3(-halfCellSize, 0 ,  halfCellSize) + gridOffset + quadOffset;
                vertices[vertQdShift + 2] = new Vector3( halfCellSize, 0 , -halfCellSize) + gridOffset + quadOffset;
                vertices[vertQdShift + 3] = new Vector3( halfCellSize, 0 ,  halfCellSize) + gridOffset + quadOffset;

                triangles[triQdShift + 0] = vertQdShift + 0;
                triangles[triQdShift + 1] = vertQdShift + 1;
                triangles[triQdShift + 2] = vertQdShift + 2;
                triangles[triQdShift + 3] = vertQdShift + 2;
                triangles[triQdShift + 4] = vertQdShift + 1;
                triangles[triQdShift + 5] = vertQdShift + 3;

                vertQdShift += 4;
                triQdShift  += 6;
            }   
        }   
    }


}
