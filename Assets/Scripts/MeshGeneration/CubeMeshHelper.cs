﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// For a Cube facing the +ve z direction, with up pointing in +ve y
/// </summary>
public enum CubeDirection
{
    forward = 0,
    right   = 1,
    back    = 2,
    left    = 3,
    up      = 4,
    down    = 5,
}

public static class CubeMeshHelper 
{
    public static Vector3[] vertices = 
    {
        new Vector3( 1, 1, 1),
        new Vector3(-1, 1, 1),
        new Vector3(-1,-1, 1),
        new Vector3( 1,-1, 1),
        new Vector3(-1, 1,-1),
        new Vector3( 1, 1,-1),
        new Vector3( 1,-1,-1),
        new Vector3(-1,-1,-1),

    };


    public static int[][] faceTriangles = 
    {
        new int[] {0,1,2,3},
        new int[] {5,0,3,6},
        new int[] {4,5,6,7},
        new int[] {1,4,7,2},
        new int[] {5,4,1,0},
        new int[] {3,2,7,6},
    };

    public static Vector3[] GetFaceVertices(int direction, float faceScale)
    {
        Vector3[] faceVertices = new Vector3[4];
        for (int fVCtr = 0; fVCtr < faceVertices.Length; fVCtr++)
        {
            faceVertices[fVCtr] = vertices[faceTriangles[direction][fVCtr]] * faceScale;
        } 
        return faceVertices;
    }
}
