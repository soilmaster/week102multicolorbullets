﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class ProcCubeGen : MonoBehaviour
{
    //Mesh variables
    Mesh mesh;
    List<Vector3> vertices;
    List<int> triangles;

    [SerializeField]
    public float scale = 1f;
 
    private float adjScale;

    // Start is called before the first frame update
    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        adjScale = scale * 0.5f;
       
    }

    // Update is called once per frame
    void Start()
    {
        // MakeProdecuralDiscreteGrid();
        MakeCube(adjScale);
        UpdateMesh();


        GetComponent<MeshCollider>().sharedMesh = mesh; 
        GetComponent<MeshCollider>().convex = true; 
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

    }


    private void MakeCube(float cubeScale)
    {
        vertices    = new List<Vector3>();
        triangles   = new List<int>();

        for (int faceCntr = 0; faceCntr < 6; faceCntr++)
        {
            MakeFace(faceCntr, cubeScale);
        }
    }
    
    private void MakeFace(int direction, float faceScale)
    {
        vertices.AddRange( CubeMeshHelper.GetFaceVertices(direction, faceScale));

        int vertStartIndex = vertices.Count - 4;

        triangles.Add(vertStartIndex + 0);
        triangles.Add(vertStartIndex + 1);
        triangles.Add(vertStartIndex + 2);
        triangles.Add(vertStartIndex + 0);
        triangles.Add(vertStartIndex + 2);
        triangles.Add(vertStartIndex + 3);
    }
}
